#! /usr/bin/env node

const app = require('../src/app.js');
const program = require('commander');

program
  .usage('[options] <name ...>')
  .arguments('<name>')
  .option('-o, --output [path]', 'relative path where to create the component. exp: spoch component MyFeature -o ./path/to ')
  .option('--routing', 'create component without router. exp: spoch component MyFeature -o ./path/to --routing')
  .action(function(name, options) {
    let output = options.output;
    let routing  = !!options.routing;
    app.createComponent({
        name: name,
        output: output,
        routing: routing
    });
  });

program.on('--help', function(){
  console.log(' examples:');
  console.log('');
  console.log('  > spoch component MyFeature -o ./path/to');
});

program.parse(process.argv);
