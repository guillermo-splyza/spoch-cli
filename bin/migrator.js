#! /usr/bin/env node

const app = require('../src/app.js');
const program = require('commander');

program
  .usage('[options] <path ...>')
  .arguments('<path>')
  .action(function(path, options) {
    app.migrateTemplate({
        path: path
    });
  });

program.on('--help', function(){
  console.log(' examples:');
  console.log('');
  console.log('  > spoch component MyFeature -o ./path/to');
});

program.parse(process.argv);
