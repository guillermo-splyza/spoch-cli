#! /usr/bin/env node

var app = require('../src/app.js');
const loadJsonFile = require('load-json-file');

const project_description = loadJsonFile.sync(__dirname + '/../package.json');

// console.log('app-bin');

var program = require('commander');
program
  .version(project_description.version)
  .command('component [name]','create angular component. by default it adds a route, mean to be used on angular-ui route')
  .parse(process.argv);

program.on('--help', function(){
  console.log(' more options ');
  console.log('');
  console.log('-o, --output', 'relative path where to create the component. exp: spoch component MyFeature -o ./path/to ');
  console.log('--not-route', 'create component without router. exp: spoch component MyFeature -o ./path/to --not-route');
});