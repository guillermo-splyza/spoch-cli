## commands

- __spoch4-component__: creates no routed component template
- __spoch4-component --routing__: creates a routed component template
- __spoch4-template-migrate__: it parse some of the old angular template properties format to the new angular template format

***

### for running commands on local

```
npm build
npm link
spoch4-component Home
spoch4-template-migrate ./template.html
```

or, from npm package download


```
npm install -g
spoch4 component Home
spoch4 template migrate ./template.html
```

if you are running windows you will need to add the path to the environment variables

#### set output dir

```
spoch4 component MyFeature -o ./path/to/
```

**the result will be something like**

```
/path/to/my-feature
/path/to/my-feature.component.html
/path/to/my-feature.component.ts
/path/to/my-feature.module.ts
```

### with routing module

```
spoch4 component MyFeature --routing
```

**the result will be something like**

```
/path/to/my-feature
/path/to/my-feature.component.html
/path/to/my-feature.component.ts
/path/to/my-feature.module.ts
/path/to/my-feature.routing.module.ts
```
### for migrating angularjs views to angular views

this command `spoch4-template-migrate` will parse many of the old angular template keywords to the new angular template format, but not all of them.

example of usage:

```
spoch4-template-migrate /path/../spoch4-v3/src/app/components/input-number/input-number.component.html
```

the following is the list of the replaced keywords:


```
-- you still need to change these ones manually --

layout-
ng-
translate-

layout-wrap
translate-attr
ng-repeat
ng-src

--- replaced properties --

 flex 
 fxFlex 

 flex=
 fxFlex=

 flex>
 fxFlex>

 layout=
 fxLayout=

 layout-align=
 fxLayoutAlign=

 ng-if=
 *ngIf=

 ng-click=
 (click)=

 ng-class
 [ngClass]

 ng-style
 [ngStyle]

 $ctrl.
```