
const ComponentBuilder = require('./component.builder.js');
const TemplateMigrator = require('./template-migrator.js');


module.exports = {
    createComponent: createComponent,
    migrateTemplate: migrateTemplate
}

function createComponent(config) {
    let builer = new ComponentBuilder();
    builer.build(config);
}

function migrateTemplate(config) {
    let migrator = new TemplateMigrator();
    migrator.parseTemplate(config);
}


