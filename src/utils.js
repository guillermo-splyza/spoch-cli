var fs = require('fs');

function getArguments(text) {
    let re = /(--[^\s]{1,})/g;
    let found = text.match(re);
    return found;
}

function formatArguemnts(textArray) {
    let results = {};
    let nodePath = textArray.shift();
    let executionFile = textArray.shift();
    let joinedText = textArray.join(' ');
    let args = getArguments(joinedText);

    for ( let i = 0, length = args.length; i < length; i++) {
        let splittedArg = splitArg(args[i]);
        if ( splittedArg && splittedArg.length === 2 ) {
            results[splittedArg[0]] = splittedArg[1];
        } else if ( splittedArg && splittedArg.length > 2 ) {
            results[splittedArg[1]] = splittedArg[2];
        }
    }
    results['executionFile'] = executionFile;
    results['nodePath'] = nodePath;
    return results;
}

function splitArg(argText) {
    let re = /--(.{1,})=(.{1,})/i;
    let found = argText.match(re);
    return found;
}

function writeToFile(fileName, content) {

    let dir = getFileDir(fileName);
    createDirP(dir);

    fs.writeFile(fileName, content, function (err) {
        if (err) { 
            console.log(err);
            return;
        }
        const FgGreen = "\x1b[32m"
        const Reset = "\x1b[0m";
        console.log('file created at :', FgGreen, fileName, Reset);
    });
}

/**
 * create dir if not exists recursively
 */
function createDirP(dir) {
    let dirs = dir.split('/');
    let processedPath = [];
    for ( let i = 0, length = dirs.length; i < length; i++ ) {
        let currentDir = dirs[i];
        processedPath.push(currentDir);
        if ( currentDir === '' || currentDir === '..' ) { continue; }
        let tempPath = processedPath.join('/');
        if (!fs.existsSync(tempPath)) {
            fs.mkdirSync(tempPath);
        }
    }
}

function splitByUpperCase(text) {
    let re = /(.[^\A-Z]{1,})/g;
    let found = text.match(re);
    return found;
}

function firstToLower(text) {
    return text.charAt(0).toLowerCase() + text.slice(1);
}

function camelToLines(text) {
    let results = [];
    let args = splitByUpperCase(text);
    for ( let i = 0, length = args.length; i < length; i++) {
        let lowered = firstToLower(args[i]);
        results.push(lowered);
    }
    return results.join('-');
}

function getFileDir(text) {
    let re = /(.{1,})(\/[^\/]{1,}$)/i;
    let found = text.match(re);
    if ( found && found.length === 3 ) {
        return found[1];
    }
    return "";
}

module.exports = {
    formatArguemnts: formatArguemnts,
    writeToFile: writeToFile,
    firstToLower: firstToLower,
    camelToLines: camelToLines
}
