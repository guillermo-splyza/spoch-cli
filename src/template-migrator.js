const { match } = require('assert');
const fs = require('fs');
const utils = require('./utils.js');

module.exports =  TemplateMigrator;

function TemplateMigrator() { }

TemplateMigrator.prototype.parseTemplate = function(config) {
    parseTemplate(config);
}

function parseData(config) {
    let data = {};
    data.path = config.path;

    return data;
}

function parseTemplate(config) {

    const vars = parseData(config);

    var stream = fs.createReadStream(vars.path);
    var found = false;
    stream.on('data',function(d){
        const fileData = (''+d);
        const parsedData = executeParse(fileData);
        if ( parsedData ) {
            fs.writeFile(vars.path, parsedData, 'utf-8', function(err, data) {
                if (err) throw err;
                console.log('Done!');
            })
        }
    });
    stream.on('error',function(err) {
        throw new Error(err);
    });
    stream.on('close',function(err) {
        console.error(err, found);
    });
}

function executeParse(fileBody) {
    // {regex: /aaaaa/g, val: 'aaaa'}
    var regexData = [
        {regex: / flex /g, val: ' fxFlex '},
        {regex: / flex=/g, val: ' fxFlex='},
        {regex: / flex>/g, val: ' fxFlex>'},
        {regex: / layout=/g, val: ' fxLayout='},
        {regex: / layout-align=/g, val: ' fxLayoutAlign='},
        {regex: / ng-if=/g, val: ' *ngIf='},
        {regex: / ng-click=/g, val: ' (click)='},
        {regex: / ng-class/g, val: ' [ngClass]'},
        {regex: / ng-style/g, val: ' [ngStyle]'},
        {regex: / translate-values=/g, val: ' [translateParams]='},
        {regex: /\$ctrl\./g, val: ''}
    ]

    var matchResult = fileBody;

    for ( var i = 0; i < regexData.length; i++ ) {
        let item = regexData[i];
        matchResult = matchResult.replace(item.regex, item.val);
    }

    return matchResult;
}

