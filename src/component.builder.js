
const utils = require('./utils.js');
const ejs = require('ejs');

module.exports =  ComponentBuilder;

function ComponentBuilder() { }

ComponentBuilder.prototype.build = function(config) {
    createComponent(config);
}

function createComponent(config) {
    let data = parseData(config);
    createFileFromTemplate(data, 'component-js.ejs', 'component.ts');
    createFileFromTemplate(data, 'component-html.ejs', 'component.html');
    createFileFromTemplate(data, 'module.ejs', 'module.ts');
    if ( data.routing ) {
        createFileFromTemplate(data, 'router.ejs', 'routing.module.ts');
    }
}

function parseData(config) {
    console.log(config);
    let data = {};
    data.name = config.name || 'Empthy';
    data.nameFirstLower = utils.firstToLower(data.name);
    data.nameLined = utils.camelToLines(data.name);
    data.output = config.output || null;
    data.routing = config.routing || false;

    if ( data.name === '' || !data.name ) {
        throw new Error('you need to provide a name for the component ');
    }

    return data;
}

function createFileFromTemplate(data, templateName, outputFileName) {
    let template = __dirname + "/templates/" + templateName;
    let outputFullFileName =  makeOutputFileFullPath(outputFileName, data);

    ejs.delimiter = '%';
    ejs.renderFile(template, data, function(err, str){

        if ( str === undefined ) {
            throw new Error('template could not be parsed: ');
        }

        utils.writeToFile(outputFullFileName, str);
    });
}

function makeOutputFileFullPath(outputFileName, data) {
    let fileName =  data.nameLined + '/' + data.nameLined +  '.' + outputFileName;
    let output = data.output;

    if ( output ) {
        let outputLength = output.length;
        if ( typeof output === 'string' && outputLength > 0 ) {
            if ( outputLength > 1 && output.slice(0, 2) === './' ) {
                return data.output + '/' + fileName;
            } else if ( output.slice(0, 1) === '/' ) {
                return data.output + '/' + fileName;
            }
            else {
                return process.cwd() + '/' +  data.output + '/' + fileName;
            }
        }
    }

    return './' + fileName;
}